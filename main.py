#!/usr/bin/python3

import random

class Exercise:
    def __init__(self):
        self.correct=0
        self.wrong=0

    def main(self):
        print("1) Sum")
        print("2) Substraction")
        print("3) Multiplication")
        print("4) Division")
        exercise=input("Select an exercise: ")
        exercise=int(exercise)
        if exercise == 1: self.sum()
        elif exercise == 2: self.substraction()
        elif exercise == 3: self.multiplication()
        elif exercise == 4: self.division()
        else: return self.main()

    def getNumbers(self, min=1,max=10):
        return (random.randint(min,max), random.randint(min,max))

    def printStats(self):
        print("Correct: "+str(self.correct))
        print("Wrong: "+str(self.wrong))

    def sum(self):
        x, y=self.getNumbers()
        val=input(str(x)+"+"+str(y)+": ")
        if val == "s":
            self.printStats()
            return
        elif x+y == int(val):
            self.correct+=1
            print("Correct answer")
            return self.sum()
        else:
            self.wrong+=1
            print("Wrong answer, was "+str(x+y))
            return self.sum()
    
    def substraction(self):
        x, y=self.getNumbers()
        val=input(str(x)+"-"+str(y)+": ")
        if val == "s":
            self.printStats()
            return
        elif x-y == int(val):
            self.correct+=1
            print("Correct answer")
            return self.substraction()
        else:
            self.wrong+=1
            print("Wrong answer, was "+str(x-y))
            return self.substraction()

    def multiplication(self):
        x, y=self.getNumbers()
        val=input(str(x)+"*"+str(y)+": ")
        if val == "s":
            self.printStats()
            return
        elif x*y == int(val):
            self.correct+=1
            print("Correct answer")
            return self.multiplication()
        else:
            self.wrong+=1
            print("Wrong answer, was "+str(x*y))
            return self.multiplication()

    def division(self):
        x, y=self.getNumbers()
        val=input(str(x)+"/"+str(y)+": ")
        if val == "s":
            self.printStats()
            return
        elif x/y == float(val):
            self.correct+=1
            print("Correct answer")
            return self.division()
        else:
            self.wrong+=1
            print("Wrong answer, was "+str(x/y))
            return self.division()

test=Exercise()
test.main()